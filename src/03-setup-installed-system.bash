#!/usr/bin/env bash

set -o errexit  # exit when a command fails
set -o pipefail # return the exit status of the last command that threw a non-zero exit code
set -o nounset  # exit when script tries to use undeclared variables

main() {
  # TODO validate output of the following commands
  #   - cat /etc/crypttab
  #   - cat /etc/fstab
  #   - sudo mount -av
  #   - sudo mount -v | grep /dev/mapper
  #   - sudo swapon
  #   - sudo btrfs filesystem show /
  #   - sudo btrfs subvolume list /
  update_and_upgrade_system
  if [[ ! ${NO_SSD:-} ]]; then
    discard_blocks_not_in_use
  fi

  install_autosnap_and_grub_btrfs

  print_success "\n\nDone. Please reboot."
}

update_and_upgrade_system() {
  apt update -y
  apt upgrade -y
  apt dist-upgrade -y
  apt autoremove -y
  apt autoclean -y
}

discard_blocks_not_in_use() {
  # discards ("trim") blocks that are not in use by the filesystem once a week – useful for solid-state drives (SSDs)
  # TODO Once we're sure to use the Linux Kernel >= 5.6, this can be replaced with the discard setting in /etc/crypttab
  sudo systemctl enable fstrim.timer
}

install_autosnap_and_grub_btrfs() {
  apt install btrfs-progs git make
  clone_and_install wmutschl/timeshift-autosnap-apt
  turn_off_autosnap_boot_folder_backup
  clone_and_install Antynea/grub-btrfs
}

clone_and_install() {
  local id="${1}"
  local prefix="${2:-https://github.com/}"
  local suffix="${3:-.git}"
  local folder="${id##*/}"
  if [[ ! -d "${HOME:?}/${folder}" ]]; then
    git clone "${prefix}${id}${suffix}" "${HOME:?}/${folder}"
    (
      cd "${HOME:?}/${folder}" && \
      sudo make install
    )
    rm -rf "${HOME:?}/${folder}"
  fi
}

turn_off_autosnap_boot_folder_backup() {
  # We don't have a dedicated /boot partition.
  # Notice: the EFI partition will still be synced to /boot.backup/efi.
  sudo sed --in-place 's/snapshotBoot=true/snapshotBoot=false/' /etc/timeshift-autosnap-apt.conf
}

print_success() {
  echo -e "\033[1;32m$*\033[0m"
}

main "$@"
