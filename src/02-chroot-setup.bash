#!/usr/bin/env bash

set -o errexit  # exit when a command fails
set -o pipefail # return the exit status of the last command that threw a non-zero exit code
set -o nounset  # exit when script tries to use undeclared variables

mount_all_and_setup_chroot_crypttab() {
    local device="${1}"
    local swap_partition="${2}"
    local system_partition="${3}"
    local mapped_name="${4}"
    local key="${5}"

    local luks_folder="/etc/luks"
    local key_file_suffix=".keyfile"
    local key_file="${luks_folder}/boot_os${key_file_suffix}"

    mount_all_partitions "${mapped_name}"

    prevent_duplicated_login "${system_partition}" "${key}" "${luks_folder}" "${key_file_suffix}" "${key_file}"
    local swap_mapped_name="cryptswap"
    create_crypttab "${swap_mapped_name}" "${swap_partition}" "${mapped_name}" "${system_partition}" "${key_file}"
    adapt_fstab "${swap_partition}"

    install_efi_bootloader "${device}" "${mapped_name}"

    local backup_device_uuid
    local parent_device_uuid
    backup_device_uuid=$(get_uuid "/dev/mapper/${mapped_name}")
    parent_device_uuid=$(get_uuid "/dev/${system_partition}")
    setup_timeshift "${backup_device_uuid}" "${parent_device_uuid}"
}

mount_all_partitions() {
  local mapped_name="${1}"
  mount --all
  assert_mounted "/dev/mapper/${mapped_name}" "/" "/@"
  assert_mounted "/dev/mapper/${mapped_name}" "/home" "/@home"
}

assert_mounted() {
  local device="${1}"
  local mount_point="${2}"
  local subvolume="${3}"
  local mounts
  mounts=$(mount)
  (echo $mounts | grep --quiet --extended-regexp "${device}.+${mount_point}.+${subvolume}") \
    || exit_with_error "Expected ${device} to be mounted on ${mount_point} as subvol ${subvolume} but got\n\n${mounts}"
}

exit_with_error() {
  print_error "${1}"
  exit 1
}

print_error() {
  (echo >&2 -e "\033[91m$*\033[0m")
}

print_warning() {
  echo -e "\033[1;33m$*\033[0m"
}

create_crypttab() {
  local swap_mapped_name="${1}"
  local swap_partition="${2}"

  local system_mapped_name="${3}"
  local system_partition="${4}"

  local key_file="${5}"
  backup_unexpected /etc/crypttab

  local uuid_swap
  local uuid

  uuid_swap=$(get_uuid "/dev/${swap_partition}")
  echo "${swap_mapped_name} UUID=${uuid_swap} /dev/urandom swap,offset=1024,cipher=aes-xts-plain64,size=512" \
    > /etc/crypttab

  uuid=$(get_uuid "/dev/${system_partition}")
  echo "${system_mapped_name} UUID=${uuid} ${key_file} luks" \
    >> /etc/crypttab
}

get_uuid() {
  local device="${1}"
  blkid -s UUID -o value "${device}"
}

backup_unexpected() {
  local file="${1}"
  if [[ -f "${file}" ]]; then
    print_warning "Did not expect ${file} to exist. Existing ${file} will be ignored."
    backup "${file}"
  fi
}

backup() {
  local file="${1}"
  print_warning "Backing up ${file} to ${file}.bak"
  cp "${file}" "${file}.bak"
}

adapt_fstab() {
  local swap_partition="${1}"
  local uuid_swap

  backup /etc/fstab

  echo "adapting /etc/fstab"

  uuid_swap=$(get_uuid "/dev/${swap_partition}")
  sed --in-place "s|UUID=${uuid_swap}|/dev/mapper/cryptswap|" /etc/fstab
}

prevent_duplicated_login() {
  local system_partition="${1}"
  local original_key="${2}"
  local luks_folder="${3}"
  local key_file_suffix="${4}"
  local key_file="${5}"
  local target_slot="1"

  # set up temporary keyfile for original password in RAM
  local original_key_file="/dev/shm/original_key_file"
  echo -n "${original_key}" > "${original_key_file}"

  prepare_key_file_creation "${luks_folder}" "${key_file}"
  erase_key_slot_if_present "${target_slot}" "${system_partition}" "${original_key_file}"

  create_key_file "${luks_folder}" "${key_file}"
  set_key_slot "${system_partition}" "${original_key_file}" "${key_file}" "${target_slot}"

  harden_initramfs_security_options "${luks_folder}/*${key_file_suffix}"
}

prepare_key_file_creation() {
  local luks_folder="${1}"
  local key_file="${2}"
  if [[ ! -d "${luks_folder}" ]]; then
    mkdir "${luks_folder}"
  fi
  backup_unexpected "${key_file}"
}

create_key_file() {
  local luks_folder="${1}"
  local key_file="${2}"
  dd if=/dev/urandom of="${key_file}" bs=4096 count=1
  chmod u=rx,go-rwx "${luks_folder}"
  chmod u=r,go-rwx "${key_file}"
}

erase_key_slot_if_present() {
  local slot_to_kill="${1}"
  local system_partition="${2}"
  local original_key_file="${3}"

  if cryptsetup luksDump "/dev/${system_partition}" | grep "Key Slot ${slot_to_kill}: ENABLED"; then
    print_warning "Key slot ${slot_to_kill} in use. Trying to kill it using the temporary keyfile ${original_key_file}"
    cryptsetup --key-file "${original_key_file}" luksKillSlot "/dev/${system_partition}" "${slot_to_kill}" \
      || exit_with_error "Could not kill occupied key slot ${slot_to_kill}. This is fatal, since we need a deterministic slot for our second keyfile, in order to stay idempotent."
  fi
}

set_key_slot() {
  local system_partition="${1}"
  local original_key_file="${2}"
  local new_key_file="${3}"
  local target_slot="${4}"
  cryptsetup --key-file="${original_key_file}" --key-slot "${target_slot}" luksAddKey "/dev/${system_partition}" "${new_key_file}" \
  || (print_error "${original_key_file}" && cat "${original_key_file}" && exit 1)
}

harden_initramfs_security_options() {
  local key_file_pattern="${1}"

  append_line_if_not_existing "KEYFILE_PATTERN=${key_file_pattern}" "/etc/cryptsetup-initramfs/conf-hook"
  append_line_if_not_existing "UMASK=0077" "/etc/initramfs-tools/initramfs.conf"
}

append_line_if_not_existing() {
  local line="${1}"
  local file="${2}"
  if ! grep --fixed-strings "${line}" "${file}"; then
    echo "${line}" >> "${file}"
  fi
}

install_efi_bootloader() {
  local device="${1}"
  local mapped_name="${2}"

  append_line_if_not_existing "GRUB_ENABLE_CRYPTODISK=y" /etc/default/grub

  prepare_system_for_grub_target_x86_64_efi

  create_new_initramfs_for_all_installed_kernel_versions

  grub-install "/dev/${device}" || exit_with_error "grub install failed on /dev/${device}"
  update-grub

  assert_permissions /boot/initrd.img '-rw-------'

  assert_initramfs_includes_keyfile "cryptroot/keyfiles/" "${mapped_name}.key"
}

prepare_system_for_grub_target_x86_64_efi() {
  # Some distros don't have this package installed.
  # We need this package for grub to automatically
  # choose x86_64-efi. Otherwise it'll resort to
  # target i386-pc and fail. The package seems
  # to be missing oftentimes.
  apt install -y grub-efi-amd64-signed
}

create_new_initramfs_for_all_installed_kernel_versions() {
  # -c create a new initramfs
  # -k all – in combination with -c:
  # operate on all installed kernel versions
  update-initramfs -c -k all
}

assert_permissions() {
  local file="${1}"
  local expected_permissions="${2}" # e.g. '-rw-------'
  local permissions
  permissions=$(stat -L -c "%A" "${file}")
  if [[ $permissions != "${expected_permissions}" ]]; then
    print_error "Expected ${file} permissions to be ${expected_permissions} but encountered ${permissions}. Quitting."
    exit 1
  fi
}

assert_initramfs_includes_keyfile() {
  local path="${1}" # e.g. cryptroot/keyfiles/
  local file_in_path="${2}" # e.g. "${mapped_name}.key
  local keyfiles
  keyfiles=$(lsinitramfs /boot/initrd.img | grep "^${path}")
  if [[ $keyfiles != "${path}${file_in_path}" ]]; then
    print_error "Keyfile ${path}${file_in_path} not contained in initramfs. Quitting."
    exit 1
  fi
}

setup_timeshift() {
  local backup_device_uuid="${1}"
  local parent_device_uuid="${2}"
  apt install -y timeshift
  setup_timeshift_config
  enable_delayed_snapshot_on_boot
  enable_hourly_check_for_scheduled_snapshots
}

setup_timeshift_config() {
  cat <<JSON > /etc/timeshift/timeshift.json
{
  "backup_device_uuid" : "${backup_device_uuid}",
  "parent_device_uuid" : "${parent_device_uuid}",
  "do_first_run" : "false",
  "btrfs_mode" : "true",
  "include_btrfs_home_for_backup" : "true",
  "include_btrfs_home_for_restore" : "false",
  "stop_cron_emails" : "true",
  "btrfs_use_qgroup" : "true",
  "schedule_monthly" : "true",
  "schedule_weekly" : "true",
  "schedule_daily" : "true",
  "schedule_hourly" : "false",
  "schedule_boot" : "true",
  "count_monthly" : "1",
  "count_weekly" : "3",
  "count_daily" : "5",
  "count_hourly" : "6",
  "count_boot" : "3",
  "snapshot_size" : "0",
  "snapshot_count" : "0",
  "date_format" : "%Y-%m-%d %H:%M:%S",
  "exclude" : [
  ],
  "exclude-apps" : [
  ]
}
JSON
}

enable_delayed_snapshot_on_boot() {
  cat <<BASH > /etc/cron.d/timeshift-boot
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=""

@reboot root sleep 10m && timeshift --create --scripted --tags B
BASH
}

enable_hourly_check_for_scheduled_snapshots() {
  cat <<BASH > /etc/cron.d/timeshift-hourly
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=""

0 * * * * root timeshift --check --scripted
BASH
}

mount_all_and_setup_chroot_crypttab "$@"
