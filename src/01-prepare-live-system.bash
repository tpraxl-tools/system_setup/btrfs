#!/usr/bin/env bash

set -o errexit  # exit when a command fails
set -o pipefail # return the exit status of the last command that threw a non-zero exit code
set -o nounset  # exit when script tries to use undeclared variables

# noatime: prevent frequent disk writes by not storing the last access time
# space_cache: quicken caching of block group by allowing to store free space cache on the disk
# space_cache=v2 was introduced, because a kernel update made the ssd mount fail with the default v1
# commit=120: time interval in which data is written to the fs (120 taken from Manjaro)
# compress=zstd: Based on Phoronix test cases, zstd seems to best the other options: lzo, zstd & zlib
COMMON_MOUNT_OPTIONS="noatime,space_cache=v2,commit=120,compress=zstd"
# ssd: optimal use on SSD and NVME
SSD_MOUNT_OPTIONS="ssd,${COMMON_MOUNT_OPTIONS}"

MOUNT_OPTIONS="${SSD_MOUNT_OPTIONS}"

if [[ ${NO_SSD:-} ]]; then
  # don't optimize for ssd if the user exported NO_SSD
  MOUNT_OPTIONS="${COMMON_MOUNT_OPTIONS}"
fi

CHROOT_SETUP_FILE="02-chroot-setup.bash"

main() {
  local device
  local keyfile
  local mapped_name="cryptdata"

  verify_efi_mode || exit_with_error "Please boot in EFI mode"
  device=$(validate_selected_device "${1-}")
  keyfile=$(validate_keyfile "${2-}")
  verify_existance_of_mandatory_files

  prepare_installation "${device}" "${keyfile}" "${mapped_name}"

  echo "Launching ubiquity installer"

  print_emphasized "Please follow the instructions below:"
  echo "Select language & keyboard layout."
  echo "Installation type: Choose 'Something else'."
  echo "Select /dev/${device}1 -> Change -> Use as EFI System Partition"
  echo "Select /dev/${device}2 -> Change -> Use as swap area"
  echo "Select /dev/mapper/${mapped_name} Type btrfs -> Change -> Use as 'btrfs journaling filesystem'"
  printf "\t check 'Format the partition' and use '/' as mount point\n"

  print_emphasized "DO NOT REBOOT AFTERWARDS!"

  ubiquity --no-bootloader

  local key
  key=$(cat "${keyfile}")
  post_installation_setup "${device}" "${mapped_name}" "${key}"

  print_success "\n\nDone. Please reboot."
}

prepare_installation() {
  local device="${1}"
  local keyfile="${2}"
  local mapped_name="${3}"

  local boot_partition="${device}1"
  local system_partition="${device}3"

  offer_last_chance_to_cancel "${device}" 10

  create_partitions "${device}" || (free_disk "${mapped_name}" "${device}2" && create_partitions "${device}")
  format_boot "${boot_partition}"
  encrypt_and_open "${keyfile}" "${system_partition}" "${mapped_name}"

  mkfs.btrfs -L root "/dev/mapper/${mapped_name}" || exit_with_error "Could not create btrfs on ${system_partition} (/dev/mapper/${mapped_name})"

  modify_partman_for_mount_optimization

  echo "Preparations done."
  echo "Keeping /dev/mapper/${mapped_name} open."
  output_luks_close_instructions "${mapped_name}"
}

output_luks_close_instructions() {
  local mapped_name=${1}
  echo "If you wish to close it, call:"
  echo "cryptsetup luksClose \"/dev/mapper/${mapped_name}\""
}

verify_efi_mode() {
  mount | grep efivars &>/dev/null
}

exit_with_error() {
  print_error "${1}"
  exit 1
}

print_error() {
  (echo >&2 -e "\033[91m$*\033[0m")
}

validate_selected_device() {
  local device="${1-}"
  if [[ -z "${device}" ]]; then
    output_missing_device_parameter
    exit 1
  fi
  verify_device_exists "${device}"
  echo "${device}"
}

validate_keyfile() {
  local keyfile="${1-}"
  if [[ -z "${keyfile}" ]]; then
    exit_with_error "You need to specify a file, containing the disk encryption password."
  fi
  if [[ ! -f "${keyfile}" ]]; then
    exit_with_error "The file '${keyfile}' does not exist."
  fi
  if contains_newline_character "${keyfile}"; then
    exit_with_error "The keyfile ${keyfile} must not contain newline characters."
  fi
  echo "${keyfile}"
}

contains_newline_character() {
  local file="${1}"
  local number_of_lines
  number_of_lines=$( count_lines "${file}" )
  if [[ "${number_of_lines}" == "0" ]]; then
    return 1
  fi
}

count_lines() {
  local file="${1}"
  cat "${file}" | wc -l | xargs
}

verify_existance_of_mandatory_files() {
  if [[ ! -f ${CHROOT_SETUP_FILE} ]]; then
    exit_with_error "Missing ${CHROOT_SETUP_FILE}. You should copy it next to this script."
  fi
}

output_missing_device_parameter() {
  print_error "You need to specify a device name to format. See NAME column of lsblk --list for available devices:"
  lsblk --list >&2
}

output_non_existing_device() {
  print_error "You have specified an unknown device. See NAME column of lsblk --list for available devices:"
  lsblk --list >&2
}

verify_device_exists() {
  local device="${1}"
  if ! exists_device "${device}"; then
    output_non_existing_device
    exit 1
  fi
}

exists_device() {
  local device="${1}"
  lsblk --list |
    awk '{print $1}' |
    grep --invert-match NAME |
    grep "${device}$" &>/dev/null
}

offer_last_chance_to_cancel() {
  local device="${1}"
  local timeout="${2}"
  print_emphasized "\nAbout to format and partition device ${device}!"
  print_emphasized "All data will be lost.\n"
  print_emphasized "Hit CTRL+C to cancel.\n"
  while [[ $timeout -gt 0 ]]; do
    echo -n "${timeout}…"
    timeout=$((timeout - 1))
    sleep 1
  done
  echo ""
}

print_warning() {
  echo -e "\033[1;33m$*\033[0m"
}

print_emphasized() {
  echo -e "\033[1;36m$*\033[0m"
}

print_success() {
  echo -e "\033[1;32m$*\033[0m"
}

create_partitions() {
  local device="${1}"
  parted --script "/dev/${device}" \
    mklabel gpt \
    mkpart primary 1MiB 513MiB \
    mkpart primary 513MiB 4609MiB \
    mkpart primary 4609MiB 100% \
    print \
    quit
}

free_disk() {
  local mapped_name="${1}"
  local swap_partition="${2}"
  luks_close "${mapped_name}" || echo "Unsuccessful attempt to close luks partition. Maybe there was no open luks partition."
  turn_off_swap "${swap_partition}" || echo "Unsuccessful attempt to turn off swap"
}

turn_off_swap() {
  local swap_partition="${1}"
  # sometimes, the live system uses an already existing swap partition
  swapoff "/dev/${swap_partition}" || \
     (
        print_error "Failed to turn swap off on /dev/${swap_partition}"; \
        print_error "Listing used swaps, maybe you need to turn them off manually (swapoff):"
        swapon
     )
}

format_boot() {
  local boot_partition="${1}"
  # The device seems not to be available immediately. We wait for it.
  while [[ ! -b "/dev/${boot_partition}" ]]; do
    sleep 1
  done
  mkfs.fat -F32 "/dev/${boot_partition}"
}

encrypt_and_open() {
  local keyfile="${1}"
  local system_partition="${2}"
  local mapped_name="${3}"
  encrypt_system_partition "${keyfile}" "${system_partition}"
  luks_open "${keyfile}" "${system_partition}" "${mapped_name}"
}

encrypt_system_partition() {
  local keyfile="${1}"
  local system_partition="${2}"
  cryptsetup --batch-mode luksFormat --type=luks1 "/dev/${system_partition}" "${keyfile}"
}

luks_open() {
  local keyfile="${1}"
  local system_partition="${2}"
  local mapped_name="${3}"
  cryptsetup --key-file "${keyfile}" luksOpen "/dev/${system_partition}" "${mapped_name}"
}

luks_close() {
  local mapped_name="${1}"
  cryptsetup luksClose "/dev/mapper/${mapped_name}"
}

modify_partman_for_mount_optimization() {
  # add mount_options to lines 24 and 31 of mount.d/70btrfs (options for mounting @ and @home)
  optimize_mount_options "/usr/lib/partman/mount.d/70btrfs"
  # add mount_options to lines 31, 32 and 37 of fstab.d/btrfs (options for mounting @ and @home)
  local partman_fstab_d_btrfs="/usr/lib/partman/fstab.d/btrfs"
  optimize_mount_options "${partman_fstab_d_btrfs}"
  # the pass flag for fschk in fstab is useless for btrfs and should be set to 0
  set_pass_to_0 "${partman_fstab_d_btrfs}"
  set_fstab_pass_flag_to_0 "${partman_fstab_d_btrfs}"
}

optimize_mount_options() {
  local file_to_modify="${1}"
  local mount_options="${MOUNT_OPTIONS}"
  if are_mount_points_optimized "${file_to_modify}"; then
    print_warning "Will not modify mount options in ${file_to_modify}. Seems to already meet our expectations."
    return 0
  fi
  # shellcheck disable=SC2016
  local unexpanded_search='(^\s*(home_)?options="\$\{options:\+\$options,}subvol=@(home)?)"$'
  sed --in-place --regexp-extended \
    "s#${unexpanded_search}#\1,${mount_options}\"#g" \
    "${file_to_modify}"
  are_mount_points_optimized "${file_to_modify}" ||
    exit_with_error "Could not append optimization options to lines of ${file_to_modify}."
}

are_mount_points_optimized() {
  local file="${1}"
  grep --quiet "subvol=@,${MOUNT_OPTIONS}" "${file}" &&
    grep --quiet "subvol=@home,${MOUNT_OPTIONS}" "${file}"
}

set_pass_to_0() {
  local file="${1}"
  if has_expected_pass_to_0_assignments "${file}"; then
    print_warning "Not setting pass=0 in ${file}. Seems to already meet our expectations."
    return 0
  fi
  sed --in-place --regexp-extended "s#pass=(1|2)#pass=0#g" "${file}"
  has_expected_pass_to_0_assignments "${file}" \
    || exit_with_error "Could not set all pass=0 in ${file}. The file seems to have an unexpected content."
}

has_expected_pass_to_0_assignments() {
  local file="${1}"
  [[ $(number_of_pass_to_0_assignments "${file}") == 3 ]]
}
number_of_pass_to_0_assignments() {
  local file="${1}"
  grep --count --extended-regexp "pass.+0" "${file}" ||
    return 0
}

set_fstab_pass_flag_to_0() {
  local file="${1}"
  # shellcheck disable=SC2016
  local unexpanded_line_head='echo "$home_path" "$home_mp" btrfs "$home_options"'
  local flags_to_search='0 2'
  local flags_to_set='0 0'
  if grep --quiet "${unexpanded_line_head} ${flags_to_set}" "${file}"; then
    print_warning "Not adapting fstab fschk flag. It already is as expected."
    return 0
  fi
  sed --in-place \
    "s#${unexpanded_line_head} ${flags_to_search}#${unexpanded_line_head} ${flags_to_set}#" \
    "${file}"
  grep --quiet "${unexpanded_line_head} ${flags_to_set}" "${file}" \
    || exit_with_error "Could not set the pass flags to 0 0 in ${file}."
}

post_installation_setup() {
  local device="${1}"
  local mapped_name="${2}"
  local key="${3}"
  local swap_partition="${device}2"
  local system_partition="${device}3"
  local chroot_folder
  chroot_folder=$(create_chroot_environment "${mapped_name}")

  trap cleanup_chroot_environment EXIT
  chroot "${chroot_folder}" bash "/${CHROOT_SETUP_FILE}" "${device}" "${swap_partition}" "${system_partition}" "${mapped_name}" "${key}"
}

create_chroot_environment() {
  local mapped_name="${1}"
  mount -o "subvol=@,${MOUNT_OPTIONS}" "/dev/mapper/${mapped_name}" /mnt
  for i in /dev /dev/pts /proc /sys /run; do
    # remount, so that it is available in both places.
    mount --bind "${i}" "/mnt${i}";
  done
  cp /etc/resolv.conf /mnt/etc/
  cp "${CHROOT_SETUP_FILE}" /mnt/
  echo "/mnt"
}

cleanup_chroot_environment() {
  echo "Cleaning up chroot environment. Unmounting rebound mountpoints."
  rm "/mnt/${CHROOT_SETUP_FILE}"
  mount | grep /mnt | awk '{print $3}' | sort --reverse | uniq | xargs umount -l
}

main "$@"
