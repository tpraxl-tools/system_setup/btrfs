= Change Log

All noteable changes to this project will be documented in this file.
This project adheres to http://semver.org/[Semantic Versioning].

== [0.4.0] - 2023-09-22

- fix mount options for btrfs ssd: `space_cache` defaulted to v1 and broke an existing ssd mount. It also failed to setup the system. Chose `space_cache=v2` for the win

== [0.3.0] – 2022-04-01

- fix(03-setup-installed-system.bash): crashed while determining if `NO_SSD` is set. Before the fix, the check triggered `nounset` if the variable was unset.

== [0.2.0] – 2022-02-05

- feat: user can disable ssd optimizations by exporting `NO_SSD=1`

== [0.1.0] – 2022-01-23

- provide basic functionality
